﻿Imports Objets100Lib

Module ProcessDocument
    Const CST_PRISE_AU_STOCK = "Pris au stock"
    Function CreeDocumentVente(ByRef baseCommerciale As BSCIALApplication3, _
                               ByVal typeDocument As DocumentType, _
                               ByVal codeClient As String, _
                               ByVal dateDocument As Date, _
                               ByVal Reference As String, _
                               ByVal TX_ESCOMPTE As Double, _
                               ByVal LI_Intitule As String, _
                               ByVal ENT_LIVRAISON As String, _
                               ByVal ENT_ENC_MODE As String, _
                               Optional DO_COORD01 As String = "", _
                               Optional DO_COORD02 As String = "", _
                               Optional DO_COORD03 As String = "", _
                               Optional DO_COORD04 As String = ""
                               ) As IPMDocument

        Try

            Dim document As IPMDocument
            document = baseCommerciale.CreateProcess_Document(typeDocument)

            Dim enteteDocumentVente As IBODocumentVente3
            enteteDocumentVente = document.Document

            Dim baseComptable As BSCPTAApplication3
            baseComptable = baseCommerciale.CptaApplication

            Dim client As IBOClient3
            client = baseComptable.FactoryClient.ReadNumero(codeClient)
            Dim livraison As IBOClientLivraison3





            enteteDocumentVente.SetDefaultClient(client)
            enteteDocumentVente.DO_Ref = Reference
            enteteDocumentVente.DO_TxEscompte = TX_ESCOMPTE

            If ENT_ENC_MODE <> "Habituel" Then
                Dim Modepaiement As IBOModeleReglement
                If baseComptable.FactoryModeleReglement.ExistIntitule(ENT_ENC_MODE) Then
                    Modepaiement = baseComptable.FactoryModeleReglement.ReadIntitule(ENT_ENC_MODE)
                    enteteDocumentVente.ModeleReglement = Modepaiement
                End If
            End If

            'on parcour la liste des adresse de livraison afin de trouver celle de la commande
            Dim numadrliv As Integer

            For numadrliv = 1 To client.FactoryClientLivraison.List.Count
                'If client.FactoryClientLivraison.List(numadrliv).Facto Then
                livraison = client.FactoryClientLivraison.List(numadrliv)
                If livraison.LI_Intitule = LI_Intitule Then
                    enteteDocumentVente.LieuLivraison = livraison
                    Exit For
                End If
            Next

            If ENT_LIVRAISON Like "*Stock*" Then
                'Dim Modelivraison As IBPConditionLivraison
                Dim Exp As IBPExpedition3

                'Modelivraison = baseCommerciale.FactoryConditionLivraison.ReadIntitule(CST_PRISE_AU_STOCK)
                Exp = baseCommerciale.FactoryExpedition.ReadIntitule(CST_PRISE_AU_STOCK)
                'enteteDocumentVente.ConditionLivraison = Modelivraison
                enteteDocumentVente.Expedition = Exp
                enteteDocumentVente.Periodicite = baseCommerciale.FactoryPeriodicite.ReadIntitule("Manuelle")
            End If

            If DO_COORD01 <> "" Then enteteDocumentVente.DO_Coord(1) = DO_COORD01
            If DO_COORD02 <> "" Then enteteDocumentVente.DO_Coord(2) = DO_COORD02
            If DO_COORD03 <> "" Then enteteDocumentVente.DO_Coord(3) = DO_COORD03
            If DO_COORD04 <> "" Then enteteDocumentVente.DO_Coord(4) = DO_COORD04

            Return document

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en initialisation du document de vente : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function
    Function CreeDocumentVentePicthema(ByRef baseCommerciale As BSCIALApplication3, _
                              ByVal typeDocument As DocumentType, _
                              ByVal codeClient As String, _
                              ByVal dateDocument As Date, _
                              ByVal Reference As String, _
                              ByVal TX_ESCOMPTE As Double, _
                              ByVal LI_Intitule As String) As IPMDocument

        Try

            Dim document As IPMDocument
            ' Dim docsearch As IBODocumentVente3
            Dim client As IBOClient3
            Dim baseComptable As BSCPTAApplication3
            Dim livraison As IBOClientLivraison3

            Dim bfind As Boolean
            bfind = False
            document = Nothing

            baseComptable = baseCommerciale.CptaApplication
            client = baseComptable.FactoryClient.ReadNumero(codeClient)

            'For Each docsearch In baseCommerciale.FactoryDocumentVente.QueryTiersTypeOrderDate(client, DocumentType.DocumentTypeVenteCommande)
            ' If docsearch.DO_Ref = Reference Then bfind = True
            ' Next

            'If Not bfind Then
            document = baseCommerciale.CreateProcess_Document(typeDocument)
            Dim enteteDocumentVente As IBODocumentVente3
            enteteDocumentVente = document.Document
            enteteDocumentVente.SetDefaultClient(client)
            'on parcour la liste des adresse de livraison afin de trouver celle de la commande
            Dim numadrliv As Integer

            For numadrliv = 1 To client.FactoryClientLivraison.List.Count
                'If client.FactoryClientLivraison.List(numadrliv).Facto Then
                livraison = client.FactoryClientLivraison.List(numadrliv)
                If livraison.LI_Intitule = LI_Intitule Then
                    enteteDocumentVente.LieuLivraison = livraison
                    Exit For
                End If
            Next
            enteteDocumentVente.DO_Ref = Reference
            enteteDocumentVente.DO_TxEscompte = TX_ESCOMPTE
            enteteDocumentVente.Periodicite = baseCommerciale.FactoryPeriodicite.ReadIntitule("Picthema")

            'Else
            'document = baseCommerciale.CreateProcess_Document(typeDocument)

            ' document.Document = docsearch
            'document = baseCommerciale.FactoryDocumentVente.ReadPiece(DocumentType.DocumentTypeVenteCommande, docsearch.DO_Piece)


            '
            'IBODocumentLigne3 documentLigne = (IBODocumentLigne3)document.FactoryDocumentLigne.Create();
            '// affectation des différentes propriétés
            'documentLigne.Write();
            '
            '            End If
            Return document

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en initialisation du document de vente : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function




    Function CreeDocumentAchat(ByRef baseCommerciale As BSCIALApplication3, _
                               ByVal typeDocument As DocumentType, _
                               ByVal codeFournisseur As String, _
                               ByVal dateDocument As Date, _
                                ByVal Reference As String) As IPMDocument

        Try

            Dim document As IPMDocument
            document = baseCommerciale.CreateProcess_Document(typeDocument)

            Dim enteteDocumentAchat As IBODocumentAchat3
            enteteDocumentAchat = document.Document

            Dim baseComptable As BSCPTAApplication3
            baseComptable = baseCommerciale.CptaApplication

            Dim fournisseur As IBOFournisseur3
            fournisseur = baseComptable.FactoryFournisseur.ReadNumero(codeFournisseur)
            enteteDocumentAchat.DO_Ref = Reference
            enteteDocumentAchat.SetDefaultFournisseur(fournisseur)

            Return document

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en initialisation du document d'achat : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function

    Function AjouteLigneEntreeArticleLotSerie(ByRef document As IPMDocument, _
                                              ByVal referenceArticle As String, _
                                              ByVal quantite As Double, _
                                              Optional ByVal numeroLotSerie As String = "") As IBODocumentLigne3

        Try

            Dim enteteDocument As IBODocument3
            enteteDocument = document.Document

            Dim nouvelleLigne As IBODocumentAchatLigne3
            nouvelleLigne = enteteDocument.FactoryDocumentLigne.Create
            nouvelleLigne.SetDefaultArticleReference(referenceArticle, quantite)
            nouvelleLigne.SetDefaultRemise()

            If numeroLotSerie <> String.Empty Then
                nouvelleLigne.LS_NoSerie = numeroLotSerie
            End If

            nouvelleLigne.WriteDefault()

            Return nouvelleLigne

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en création de ligne d'entrée pour l'article " + referenceArticle + " : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function

    Function AjouteLigneSortieArticleLotSerie(ByRef document As IPMDocument, _
                                              ByVal referenceArticle As String, _
                                              ByVal quantite As Double, _
                                              Optional ByVal numeroLotSerie As String = "", _
                                              Optional ByVal Designation As String = "", _
                                              Optional ByVal PU As Double = Nothing, _
                                              Optional ByVal Remise As Double = Nothing, _
                                              Optional ByRef SpecialRef As List(Of String) = Nothing) As IBODocumentLigne3

        Try
            Dim baseCommerciale As BSCIALApplication3
            Dim enteteDocument As IBODocument3
            enteteDocument = document.Document

            'Dim nouvelleLigne As IBODocumentLigne3
            Dim nouvelleLigne As IBODocumentVenteLigne3

            nouvelleLigne = enteteDocument.FactoryDocumentLigne.Create
            baseCommerciale = enteteDocument.Stream

            If numeroLotSerie = String.Empty Then
                nouvelleLigne.SetDefaultArticleReference(referenceArticle, quantite)
                nouvelleLigne.SetDefaultRemise()
            Else
                Dim intituleDepot As String
                intituleDepot = enteteDocument.DepotStockage.DE_Intitule

                Dim lotParArticleDepot As IBOArticleDepotLot
                lotParArticleDepot = getLotArticleDepot(baseCommerciale, referenceArticle, intituleDepot, numeroLotSerie)
                nouvelleLigne.SetDefaultLot(lotParArticleDepot, quantite)
            End If

            If Designation <> "" Then
                nouvelleLigne.DL_Design = Designation
            End If




            If SpecialRef.Contains(referenceArticle) Then nouvelleLigne.DL_PrixUnitaire = PU
            If referenceArticle = "ZPICTHEMA" Then
                nouvelleLigne.DL_PoidsBrut = PU * 1000
                nouvelleLigne.DL_PoidsNet = PU * 1000
            End If
            Dim article As IBOArticle3
            article = baseCommerciale.FactoryArticle.ReadReference(referenceArticle)

            Try
                If article.InfoLibre.Item("AR_PRIXDESTOCK") <> 0 Then
                    nouvelleLigne.DL_PrixUnitaire = PU
                End If
            Catch ex As Exception

            End Try

            nouvelleLigne.WriteDefault()



            Return nouvelleLigne

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en création de ligne de sortie pour l'article " + referenceArticle + " : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function
    Function AjouteLigneSortieArticleLotSerie(ByRef enteteDocument As IBODocumentVente3, _
                                             ByVal referenceArticle As String, _
                                             ByVal quantite As Double, _
                                             Optional ByVal numeroLotSerie As String = "", _
                                             Optional ByVal Designation As String = "", _
                                             Optional ByVal PU As Double = Nothing, _
                                             Optional ByVal Remise As Double = Nothing, _
                                             Optional ByRef SpecialRef As List(Of String) = Nothing) As IBODocumentLigne3

        Try
            Dim baseCommerciale As BSCIALApplication3


            'Dim nouvelleLigne As IBODocumentLigne3
            Dim nouvelleLigne As IBODocumentVenteLigne3

            nouvelleLigne = enteteDocument.FactoryDocumentLigne.Create
            baseCommerciale = enteteDocument.Stream

            If numeroLotSerie = String.Empty Then
                nouvelleLigne.SetDefaultArticleReference(referenceArticle, quantite)
                nouvelleLigne.SetDefaultRemise()
            Else
                Dim intituleDepot As String
                intituleDepot = enteteDocument.DepotStockage.DE_Intitule

                Dim lotParArticleDepot As IBOArticleDepotLot
                lotParArticleDepot = getLotArticleDepot(baseCommerciale, referenceArticle, intituleDepot, numeroLotSerie)
                nouvelleLigne.SetDefaultLot(lotParArticleDepot, quantite)
            End If

            If Designation <> "" Then
                nouvelleLigne.DL_Design = Designation
            End If




            If SpecialRef.Contains(referenceArticle) Then nouvelleLigne.DL_PrixUnitaire = PU
            If referenceArticle = "ZPICTHEMA" Then
                nouvelleLigne.DL_PoidsBrut = PU * 1000
                nouvelleLigne.DL_PoidsNet = PU * 1000
            End If
            Dim article As IBOArticle3
            article = baseCommerciale.FactoryArticle.ReadReference(referenceArticle)

            Try
                If article.InfoLibre.Item("AR_PRIXDESTOCK") <> 0 Then
                    nouvelleLigne.DL_PrixUnitaire = PU
                End If
            Catch ex As Exception

            End Try

            nouvelleLigne.WriteDefault()



            Return nouvelleLigne

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en création de ligne de sortie pour l'article " + referenceArticle + " : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Function

    Function getLotArticleDepot(ByRef baseCommerciale As BSCIALApplication3, _
                                ByVal referenceArticle As String, _
                                ByVal intituleDepot As String, _
                                ByVal numeroLot As String) As IBOArticleDepotLot

        Dim article As IBOArticle3
        article = baseCommerciale.FactoryArticle.ReadReference(referenceArticle)

        Dim depot As IBODepot3
        depot = baseCommerciale.FactoryDepot.ReadIntitule(intituleDepot)

        Dim depotArticle As IBOArticleDepot3
        depotArticle = article.FactoryArticleDepot.ReadDepot(depot)

        Dim lotArticleDepot As IBOArticleDepotLot
        lotArticleDepot = depotArticle.FactoryArticleDepotLot.ReadNoSerie(numeroLot)

        Return lotArticleDepot

    End Function

    Sub ModifieInfoLibreEntete(ByRef enteteDocument As IBODocument3, _
                              ByVal intituleInfoLibre As String, _
                              ByVal valeurInfoLibre As Object)

        Try
            enteteDocument.InfoLibre.Item(intituleInfoLibre) = valeurInfoLibre
            enteteDocument.Write()


        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en modification d'une information libre d'une ligne : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Sub

    Function ConfirmeCommande(ByRef baseCommerciale As BSCIALApplication3, ByRef enteteDocumentG As IBODocument3, ByVal DO_TIERS As String, ByVal strCom As String, ByRef conf As Configuration) As Boolean
        Dim strtxt As String = ""
        Dim enteteDocument As IBODocumentVente3



        Dim client As IBOClient3
        Dim baseComptable As BSCPTAApplication3
        Dim bfind As Boolean = False
        Dim bToconfirme = True
        Dim email As String
        Dim StrMessage As String

        ConfirmeCommande = False

        enteteDocument = Nothing
        baseComptable = baseCommerciale.CptaApplication
        client = baseComptable.FactoryClient.ReadNumero(DO_TIERS)

        For Each enteteDocument In baseCommerciale.FactoryDocumentVente.QueryTiersTypeOrderDate(client, DocumentType.DocumentTypeVenteCommande)
            If enteteDocument.DO_Piece = enteteDocumentG.DO_Piece Then bfind = True
        Next




        StrMessage = "La commande du client " & DO_TIERS & " ne peut pas être confirmé en automatique" & "\n"

        If bfind Then

            If strCom <> "" And strCom <> "Commande WEB" Then
                bToconfirme = False
                StrMessage = StrMessage & "- Presence du commentaire : " & strCom & "\n"
            End If
            If IsClientBlocked(DO_TIERS, baseCommerciale) Then
                bToconfirme = False
                StrMessage = StrMessage & "- Client Bloqué" & "\n"
            End If
            If IsClientComptant(DO_TIERS, baseCommerciale, strtxt) Then
                bToconfirme = False
                StrMessage = StrMessage & "- Client Comptant " & strtxt & "\n"
            End If
            If IsClientExport(DO_TIERS, baseCommerciale) Then
                bToconfirme = False
                StrMessage = StrMessage & "- Client Export " & "\n"
            End If
            If IsClientGrandCompte(DO_TIERS, baseCommerciale) Then
                bToconfirme = False
                StrMessage = StrMessage & "- Client Export " & "\n"
            End If
            For Each ligne As IBODocumentLigne3 In enteteDocument.FactoryDocumentLigne.List
                If conf.ArticleAConfimer.Contains(ligne.Article.AR_Ref) Then
                    bToconfirme = False
                    StrMessage = StrMessage & "- Presence de l'article " & ligne.Article.AR_Ref & "\n"

                End If
            Next

            If bToconfirme Then
                ModifieInfoLibreEntete(enteteDocument, "Cde_traitee_par", "Confirmation automatique")
                enteteDocument.DO_Statut = DocumentStatutType.DocumentStatutTypeConfirme
                enteteDocument.Write()
                ConfirmeCommande = True
            Else
                'la commande ne peut pas etre confirmé
                'on localise  l'adresse email à utiliser
                If IsNothing(enteteDocument.Collaborateur) Then
                    email = conf.DefaultMail
                Else
                    email = enteteDocument.Collaborateur.Service
                End If

                If Not Mail.IsCorrectEmail(email) Then email = conf.DefaultMail
                'If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {email}, "Commande à confirmer " & DO_TIERS & " " & enteteDocument.DO_Piece, StrMessage, {"mail.automatique@mbtech.fr"}) Then
                If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {email}, "Commande à confirmer " & DO_TIERS & " " & enteteDocument.DO_Piece, StrMessage) Then
                    log.AddLog("ERREUR Message non envoyé : " & email & "->" & "Commande à confirmer " & DO_TIERS & " " & enteteDocument.DO_Piece & ">>>" & StrMessage)
                Else
                    log.AddLog("Message Envoyé : " & email & "->" & "Commande à confirmer " & DO_TIERS & " " & enteteDocument.DO_Piece & ">>>" & StrMessage)
                End If

            End If
        End If

    End Function

    Function AppliqueModeleEnregistrementVente(ByRef baseCommerciale As BSCIALApplication3, ByRef enteteDocument As IBODocument3) As Boolean

        Dim Lgs As IBODocumentVenteLigne3
        Dim model As IBOModele2
        Dim article As IBOArticle3
        Dim ret As Boolean

        ret = True
        ' on recupere les lignes
        For Each Lgs In enteteDocument.FactoryDocumentLigne.List
            'on trouve l'article
            article = Lgs.Article
            model = article.ModeleVente
            'on regarde si il a un modele d'enregistrement
            If Not IsNothing(model) Then
                'on l'applique ?
                Select Case model.MO_Intitule
                    Case "CORROSIF"
                        If model.MO_Calcul = "DocLigne.InfoLibValeur(1) = ""112""" Then
                            Lgs.InfoLibre(1) = "112"
                            Lgs.Write()
                        Else
                            ret = False
                        End If
                    Case "REDEVANCE"
                        If model.MO_Calcul.Replace(Chr(13), "").Replace(Chr(10), "") = "Si (CompteT.InfoLibValeur(22) = """") Ou (CompteT.InfoLibValeur(22) = "" "") Alors	InfoLibre2 = Article.InfoLibValeur(20) * DocLigne.QuantiteFinSi" Then
                            'on recherche le client
                            Dim client As IBOClient3
                            client = Lgs.Client
                            If client.InfoLibre(22) = "" Or client.InfoLibre(22) = " " Then
                                Lgs.InfoLibre(2) = article.InfoLibre(20) * Lgs.DL_Qte
                                Lgs.Write()
                            End If


                        Else
                            ret = False
                        End If



                    Case Else
                        ret = False
                End Select

            End If

        Next
        If ret Then enteteDocument.Write()
        AppliqueModeleEnregistrementVente = ret

    End Function



    Sub ValideDocument(ByRef document As IPMDocument)

        Try

            If document.CanProcess Then

                document.Process()

            Else

                Dim erreurs As String = getErreursProcess(document)

                Throw New Exception(erreurs)

            End If

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en validation du document : " + ex.Message

            Throw New Exception(erreur)

        End Try

    End Sub

End Module

