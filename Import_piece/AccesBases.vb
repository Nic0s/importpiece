﻿Imports Objets100Lib

Module AccesBases

    Function OuvreBaseComptable(ByVal chemin As String, _
                           Optional ByVal utilisateur As String = "<Administrateur>", _
                           Optional ByVal motDePasse As String = "") As BSCPTAApplication3

        Dim baseComptable As BSCPTAApplication3

        Try

            baseComptable = New BSCPTAApplication3

            With baseComptable
                .Name = chemin
                .Loggable.UserName = utilisateur
                .Loggable.UserPwd = motDePasse
                .Open()
            End With

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en ouverture de la base " + chemin + " : " + ex.Message

            Throw New Exception(erreur)

        End Try

        Return baseComptable

    End Function

    Sub FermeBaseComptable(ByRef baseComptable As BSCPTAApplication3)

        If baseComptable IsNot Nothing Then
            baseComptable.Close()
        End If

    End Sub

    Function OuvreBaseCommerciale(ByVal chemin As String, _
                              Optional ByVal utilisateur As String = "<Administrateur>", _
                              Optional ByVal motDePasse As String = "") As BSCIALApplication3

        Dim baseCommerciale As New BSCIALApplication3

        Try

            With baseCommerciale
                .Name = chemin
                .Loggable.UserName = utilisateur
                .Loggable.UserPwd = motDePasse
                .Open()
            End With

        Catch ex As Exception

            Dim erreur As String
            erreur = "Erreur en ouverture de la base " + chemin + " : " + ex.Message

            Throw New Exception(erreur)

        End Try

        Return baseCommerciale

    End Function

    Sub FermeBaseCommerciale(ByRef baseCommerciale As BSCIALApplication3)

        If baseCommerciale IsNot Nothing Then
            baseCommerciale.Close()
        End If

    End Sub



End Module

''' <summary>
''' Classe premettant la non deconnection a la base
''' 
''' </summary>
''' <remarks></remarks>

Public Class BaseCommercial
    Private m_base As BSCIALApplication3
    Private m_nbConnect As Integer
    Private m_chemin As String


    'constructeur  par defaut
    Public Sub New(ByVal chemin As String, _
                   Optional ByVal utilisateur As String = "<Administrateur>", _
                   Optional ByVal motDePasse As String = "")
        m_base = New BSCIALApplication3
        m_chemin = chemin
        Try

            With m_base
                .Name = chemin
                .Loggable.UserName = utilisateur
                .Loggable.UserPwd = motDePasse
            End With

        Catch ex As Exception
            Dim erreur As String
            erreur = "Erreur en initialisant la base " + chemin + " : " + ex.Message

            Throw New Exception(erreur)
        End Try
        m_nbConnect = 0
    End Sub

    Protected Overrides Sub Finalize()
        Ferme(True)
        m_base = Nothing
        MyBase.Finalize()
    End Sub



    Function Ouvre() As BSCIALApplication3
        If m_base Is Nothing Then
            m_nbConnect = 0
        Else
            If m_base.IsOpen() Then
                m_nbConnect = m_nbConnect + 1
            Else
                m_nbConnect = 0
            End If
        End If

        If m_nbConnect = 0 Then
            Try
                m_base.Open()
                m_nbConnect = 1
            Catch ex As Exception
                Dim erreur As String
                erreur = "Erreur en ouvrant la base " + m_chemin + " : " + ex.Message

                Throw New Exception(erreur)
            End Try
        End If

        Return m_base

    End Function

    Sub Ferme(Optional ByVal bAll As Boolean = False)
        If bAll Then
            m_nbConnect = 0
        Else
            m_nbConnect = m_nbConnect - 1
        End If

        If m_nbConnect <= 0 Then
            If m_base IsNot Nothing Then
                m_base.Close()

            Else
                m_nbConnect = 0
            End If
        End If
    End Sub
End Class

'Class BaseCompta
'    Private m_base As BSCPTAApplication3
'    Private m_nbConnect As Integer
'    Private m_chemin As String


'    'constructeur  par defaut
'    Public Sub New(ByVal chemin As String, _
'                   Optional ByVal utilisateur As String = "<Administrateur>", _
'                   Optional ByVal motDePasse As String = "")
'        m_base = New BSCPTAApplication3
'        m_chemin = chemin
'        Try

'            With m_base
'                .Name = chemin
'                .Loggable.UserName = utilisateur
'                .Loggable.UserPwd = motDePasse
'            End With

'        Catch ex As Exception
'            Dim erreur As String
'            erreur = "Erreur en initialisant la base " + chemin + " : " + ex.Message

'            Throw New Exception(erreur)
'        End Try
'        m_nbConnect = 0
'    End Sub

'    Protected Overrides Sub Finalize()
'        Ferme(True)
'        m_base = Nothing
'        MyBase.Finalize()
'    End Sub



'    Function Ouvre() As BSCPTAApplication3
'        If m_base Is Nothing Then
'            m_nbConnect = 0
'        Else
'            If m_base.IsOpen() Then
'                m_nbConnect = m_nbConnect + 1
'            Else
'                m_nbConnect = 0
'            End If
'        End If

'        If m_nbConnect = 0 Then
'            Try
'                m_base.Open()
'                m_nbConnect = 1
'            Catch ex As Exception
'                Dim erreur As String
'                erreur = "Erreur en ouvrant la base " + m_chemin + " : " + ex.Message

'                Throw New Exception(erreur)
'            End Try
'        End If

'        Return m_base

'    End Function

'    Sub Ferme(Optional ByVal bAll As Boolean = False)
'        If bAll Then
'            m_nbConnect = 0
'        Else
'            m_nbConnect = m_nbConnect - 1
'        End If

'        If m_nbConnect <= 0 Then
'            If m_base IsNot Nothing Then
'                m_base.Close()

'            Else
'                m_nbConnect = 0
'            End If
'        End If
'    End Sub

'End Class