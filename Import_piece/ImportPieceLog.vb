﻿Imports System.IO

Public Class ImportPieceLog
    Private fileLog As String

    'constructeur par defaut
    Sub New(ByVal strDir As String, ByVal strRacine As String)
        Try
            fileLog = System.IO.Path.GetDirectoryName(strDir) + System.IO.Path.DirectorySeparatorChar + strRacine + Format(Now, "yyyyMMdd") + ".txt"
        Catch ex As Exception
            'Code exécuté en cas d'exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
    Sub AddLog(ByVal strtxt As String)
        Dim Sw As StreamWriter = Nothing
        Try
            Sw = New StreamWriter(fileLog, True)
            If Not Sw Is Nothing Then
                Sw.WriteLine(Format(Now, "HH:mm:ss") + " -> " + strtxt)
            Else
                Throw New Exception("Fichier log non ouvert")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not Sw Is Nothing Then Sw.Close()
        End Try

    End Sub

   
End Class
