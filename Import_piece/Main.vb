﻿

Imports System.IO

Module Main
    Public globalmail As Mail
    Public Magestion As BaseCommercial
    Public log As ImportPieceLog

    Sub Main()

        Dim app As SingleInstanceApp = New SingleInstanceApp("MBTECHINTEGCMD2011")
        ' Dim toto


        'Dim sqldb As New SQLAccesBases("Data Source=SRVSAGE;Initial Catalog=L100_MBTECH;Trusted_Connection=True;")
        'sqldb.open()
        'toto = sqldb.GetOneResultValue("Select LI_Intitule From F_LIVRAISON where LI_no=11")
        'Mail.sendmail("n.molders@mbeditions.fr", "nmolders@hotmail.com", "test", "test in-v3.mailjet.com 25 false", "in-v3.mailjet.com", 25, False, "8a13d69083469d72005931d7c81449e2", "0b927ad6d2fd2451fb64e72243edde40")
        'Mail.sendmail("n.molders@mbeditions.fr", "nmolders@hotmail.com", "test", "test in-v3.mailjet.com 465 true", "in-v3.mailjet.com", 465, True, "8a13d69083469d72005931d7c81449e2", "0b927ad6d2fd2451fb64e72243edde40")
        'Mail.sendmail("n.molders@mbeditions.fr", "nmolders@hotmail.com", "test", "test in-v3.mailjet.com 465 true", "in-v3.mailjet.com", 587, True, "8a13d69083469d72005931d7c81449e2", "0b927ad6d2fd2451fb64e72243edde40")

        ' <SMTP>in-v3.mailjet.com</SMTP>      <port>25</port>      <SSL>FALSE</SSL>      <Login>8a13d69083469d72005931d7c81449e2</Login>      <PassWord>0b927ad6d2fd2451fb64e72243edde40</PassWord>
        'sqldb.close()

        Try
            If Not app.IsRunning Then

                Dim conf As New Configuration("configuration.xml")
                globalmail = New Mail(conf.MailSMTP, conf.Mailport, conf.MailSsl, conf.MailLogin, conf.MailPass)
                Dim bret As Boolean

                Dim c As Commande
                Dim File_Ligne As String

                Dim nbcommande As Integer

                log = New ImportPieceLog(conf.LogDir, conf.LogPrefix)


                ' bret = globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {"n.molders@mbeditions.fr"}, "Test", "Test", , )
                'If Not bret Then
                ' log.AddLog("Message non envoyé : " & "n.molders@mbeditions.fr, 0682856337@sms.oleane.com" & "->" & "Test")

                'End If
                'Dim msg As String = ""
                ' Debug.Print(IsClientBlocked("A825", conf.Base))
                ' on initilalise les bases
                Magestion = New BaseCommercial(conf.Base, , "mbtech06!")
                'Magestion = New BaseCommercial(conf.Base, , "")

                'on recherche pour chaque repertoire la presence de fichier
                For Each Rep As RepScan In conf.RepToScan

                    nbcommande = My.Computer.FileSystem.GetFiles(
                                                          Rep.chemin,
                                                          FileIO.SearchOption.SearchTopLevelOnly, "*imp_ent*.txt").Count
                    log.AddLog(String.Format("Session d'import de {0} commande(s) dans {1}", nbcommande, Rep.chemin))
                    For Each foundFile As String In My.Computer.FileSystem.GetFiles(
                                      Rep.chemin,
                                      FileIO.SearchOption.SearchTopLevelOnly, "*imp_ent*.txt")

                        Try
                            c = New Commande(foundFile)
                            If c.InsertToSage(Magestion, conf) Then
                                log.AddLog("Import d'une piece (" & c.ENTETE_NO & ") pour le " + c.DO_TIERS + " escompte " + c.ENT_ESCPTE.ToString + " Confirmer : " + c.bConfirme.ToString)



                                'on deplace la commande vers le repertoire de sauvegarde de commande
                                File_Ligne = System.IO.Path.GetFileName(foundFile)
                                File_Ligne = File_Ligne.Substring(0, 10).Replace("ent", "lig") + File_Ligne.Substring(10)
                                File_Ligne = System.IO.Path.GetDirectoryName(foundFile) + System.IO.Path.DirectorySeparatorChar + File_Ligne
                                Try
                                    If Not My.Computer.FileSystem.DirectoryExists(conf.SavDir & Format(Now, "yyyy-MM-dd")) Then My.Computer.FileSystem.CreateDirectory(conf.SavDir & Format(Now, "yyyy-MM-dd"))
                                    My.Computer.FileSystem.MoveFile(foundFile, conf.SavDir & Format(Now, "yyyy-MM-dd") & "\" & System.IO.Path.GetFileName(foundFile))
                                    My.Computer.FileSystem.MoveFile(File_Ligne, conf.SavDir & Format(Now, "yyyy-MM-dd") & "\" & System.IO.Path.GetFileName(File_Ligne))
                                Catch e As Exception
                                    My.Computer.FileSystem.RenameFile(foundFile, System.IO.Path.GetFileName(foundFile) & ".Err Copy")
                                    My.Computer.FileSystem.RenameFile(File_Ligne, System.IO.Path.GetFileName(File_Ligne) & ".Err Copy")

                                End Try
                            Else
                                log.AddLog("ERREUR d'Import d'une piece " & c.strDO_PIECE & " (" & c.ENT_COM & ") pour le " + c.DO_TIERS)
                            End If


                        Catch ex As Exception
                            log.AddLog("ERREUR Exception !!! lors d'Import d'une piece (" & foundFile & ") ->" & ex.ToString)

                            bret = globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {"n.molders@mbeditions.fr"}, "Erreur integration commande", "ERREUR Exception !!! lors d'Import d'une piece (" & foundFile & ") " + ex.Message, {"f.de-foy@mbtech.fr"}, )
                            If Not bret Then
                                log.AddLog("Message non envoyé : " & "n.molders@mbeditions.fr, 0682856337@sms.oleane.com" & "->" & "Erreur integration commande" & ">>>" & "ERREUR Exception !!! lors d'Import d'une piece (" & foundFile & ") " + ex.Message)

                            End If
                        End Try

                    Next


                Next




            End If
        Catch ex As Exception
            Debug.Print(ex.ToString)
            Console.WriteLine(ex.ToString)
            Console.ReadKey()



        Finally
            CType(app, IDisposable).Dispose()
        End Try


    End Sub

End Module
