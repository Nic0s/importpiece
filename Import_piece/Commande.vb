﻿' classe repereantant une commande 
' des informations sur l'entete 

Imports System.Globalization
Imports Objets100Lib

Public Class Commande
    Const NB_CHAMPS_ENTETE = 14
    Const NB_CHAMPS_LIGNE = 6

    'Propiete de l'entete
    Property ENTETE_NO As String
    Property DO_DATE As Date
    Property DO_TIERS As String
    Property Li_NO As ULong
    Property ENT_ENC_TYPE As Integer
    Property ENT_ENC_DATE As Date
    Property ENT_ENC_MT As Double
    Property ENT_ENC_MODE As String
    Property ENT_LIV_DATE As Date
    Property ENT_COM As String
    Property ENT_ESCPTE As Double
    Property ENT_MAIL As Boolean

    
    Property ReferencePaiementEnLigne As String

    'DO_TYPE CORREPONDANT à la V11 de sage
    Property DO_TYPEWEB As Integer
    Property ENT_LIVRAISON As String


    'optionel juste pour les commande grand public
    Property DO_COORD01 As String
    Property DO_COORD02 As String
    Property DO_COORD03 As String
    Property DO_COORD04 As String
    Property Libre3 As String



    'les lignes
    Property LIGNES As List(Of Ligne)

    'les fichiers sources
    Private FileEntete As String
    Private FileLigne As String

    Property strDO_PIECE As String
    Property bConfirme As Boolean



    'constructeur  par defaut
    Public Sub New()
        LIGNES = New List(Of Ligne)
        bConfirme = False
    End Sub

    'constructeur 
    Public Sub New(ByVal File_Entete As String)
        MyClass.new()
        Dim csvEntete, csvLigne As CSV
        Dim ligne As String()
        Dim aLigne As Ligne
        Dim style As NumberStyles
        Dim culture As CultureInfo
        Dim i As Integer = 0
        Dim frFR As New CultureInfo("fr-FR")




        style = NumberStyles.Number Or NumberStyles.AllowCurrencySymbol
        culture = CultureInfo.CreateSpecificCulture("en-US")


        'on lit le csv d'entete

        csvEntete = New CSV(File_Entete)
        Try

            'on ne lit que la premiere ligne d'entete le reste est ignoré
            ligne = csvEntete.ReadNextLine

            'on verifie que j'ai bien le bon nombre de parametre NB_CHAMPS_ENTETE + 5 pour les commandes format gp
            If ligne.GetLength(0) <> NB_CHAMPS_ENTETE And ligne.GetLength(0) <> NB_CHAMPS_ENTETE + 5 Then
                Throw New EnteteNBChampsInvalide("Le nombre de champs du fichier entete est incorrecte")
            End If

            'le champs ENTETE_NO doit etre une chaine de au plus 19 caractere
            If ligne(i).Length > 19 Then
                Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
            Else
                ENTETE_NO = ligne(i)
            End If
            i = i + 1

            'le champs DO_DATE doit etre une date valide
            If Not Date.TryParseExact(ligne(i), "'#'yyyy'-'MM'-'dd HH':'mm':'ss'#'", frFR, DateTimeStyles.None, DO_DATE) Then
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas une date correcte")
            End If
            i = i + 1

            'le  champs DO_TIERS doit etre une chaine de au plus 17 caractere
            If ligne(i).Length > 17 Then
                Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
            Else
                DO_TIERS = ligne(i)
            End If
            i = i + 1

            'le  champs Li_NO doit etre une entier positif
            Try
                Li_NO = CULng(ligne(i).Replace("�", "").Replace("Â", ""))
            Catch ex As Exception
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un entier non signé")
            End Try
            i = i + 1

            'le  champs ENT_ENC_TYPE doit etre une entier 
            Try
                ENT_ENC_TYPE = CInt(ligne(i))
            Catch ex As Exception
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un entier")
            End Try
            i = i + 1

            'le champs ENT_ENC_DATE doit etre une date valide
            If Not Date.TryParseExact(ligne(i), "'#'yyyy'-'MM'-'dd HH':'mm':'ss'#'", frFR, DateTimeStyles.None, ENT_ENC_DATE) Then
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas une date correcte")
            End If
            i = i + 1


            'le  champs ENT_ENC_MT doit etre un double
            If Not Double.TryParse(ligne(i).Replace(",", "."), style, culture, ENT_ENC_MT) Then
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un double")
            End If
            i = i + 1

            'le  champs ENT_ENC_MODE doit etre une chaine de au plus 50 caractere
            If ligne(i).Length > 50 Then
                Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
            Else
                If ligne(i).Contains(":") Then
                    ENT_ENC_MODE = "En ligne"
                    ReferencePaiementEnLigne = Split(ligne(i), ":")(1)
                Else
                    ENT_ENC_MODE = ligne(i)
                    ReferencePaiementEnLigne = ""
                  
                End If

            End If
            i = i + 1

            'le champs ENT_LIV_DATE doit etre une date valide
            If Not Date.TryParseExact(ligne(i), "'#'yyyy'-'MM'-'dd HH':'mm':'ss'#'", frFR, DateTimeStyles.None, ENT_LIV_DATE) Then
                If Not Date.TryParseExact(ligne(i), "yyyy'-'MM'-'dd HH':'mm':'ss", frFR, DateTimeStyles.None, ENT_LIV_DATE) Then
                    Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas une date correcte")
                End If
            End If
            i = i + 1

            'le  champs ENT_COM doit etre une chaine de au plus 255 caractere
            If ligne(i).Length > 255 Then
                Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
            Else
                ENT_COM = ligne(i)
            End If
            i = i + 1

            'le  champs ENT_ESCPTE doit etre un double
            If Not Double.TryParse(ligne(i).Replace(",", "."), style, culture, ENT_ESCPTE) Then
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un double")
            End If
            i = i + 1


            'le  champs ENT_MAIL doit etre un boolean
            Dim tmpBool As Integer

            Try
                tmpBool = CInt(ligne(i))
            Catch ex As Exception
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un boolean")
            End Try
            Select Case tmpBool
                Case 1
                    ENT_MAIL = True
                Case 0
                    ENT_MAIL = False
                Case Else
                    Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un boolean")
            End Select

            i = i + 1

            'le  champs DO_TYPEWEB doit etre une entier 
            Try
                DO_TYPEWEB = CInt(ligne(i))
            Catch ex As Exception
                Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un entier")
            End Try
            i = i + 1


            'le  champs ENT_LIVRAISON doit etre une chaine de au plus 255 caractere
            If ligne(i).Length > 255 Then
                Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
            Else
                ENT_LIVRAISON = ligne(i)
            End If
            i = i + 1


            'pour les commandes formats Grand public il y a 5 champes en plus
            If ligne.GetLength(0) = NB_CHAMPS_ENTETE + 5 Then
                If ligne(i).Length > 25 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
                Else
                    DO_COORD01 = ligne(i)
                End If
                i = i + 1

                If ligne(i).Length > 25 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
                Else
                    DO_COORD02 = ligne(i)
                End If
                i = i + 1

                If ligne(i).Length > 25 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
                Else
                    DO_COORD03 = ligne(i)
                End If
                i = i + 1

                If ligne(i).Length > 25 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
                Else
                    DO_COORD04 = ligne(i)
                End If
                i = i + 1

                If ligne(i).Length > 40 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier entete a une longueur invalide")
                Else
                    Libre3 = ligne(i)
                End If
                i = i + 1

            Else
                DO_COORD01 = ""
                DO_COORD02 = ""
                DO_COORD03 = ""
                DO_COORD04 = ""
                Libre3 = ""
            End If

        Catch ex As Exception
            csvEntete.close()
            csvEntete = Nothing
            Throw ex
        Finally
            csvEntete.close()
            csvEntete = Nothing
            FileEntete = File_Entete
        End Try



        'on s'occupe des lignes
        Dim File_Ligne As String = System.IO.Path.GetFileName(File_Entete)
        File_Ligne = File_Ligne.Substring(0, 10).Replace("ent", "lig") + File_Ligne.Substring(10)
        File_Ligne = System.IO.Path.GetDirectoryName(File_Entete) + System.IO.Path.DirectorySeparatorChar + File_Ligne
        'on lit le csv d'entete
        Try
            csvLigne = New CSV(File_Ligne)
        Catch ex As Exception
            Throw New LigneIntrouvable("impossible d'ouvrir " & File_Ligne)
        End Try

        Try
            'on lit chaquen des lignes
            While Not csvLigne.EOF
                'on ne lit que la premiere ligne d'entete le reste est ignoré
                ligne = csvLigne.ReadNextLine
                aLigne = New Ligne
                'on verifie que j'ai bien le bon nombre de parametre
                If ligne.GetLength(0) <> NB_CHAMPS_LIGNE Then
                    'on verife que le dernier champ n'est pas null
                    If ligne(NB_CHAMPS_LIGNE) <> "" Then
                        Throw New EnteteNBChampsInvalide("Le nombre de champs du fichier entete est incorrecte")
                    End If
                End If
                i = 0

                'le champs LIGNE_NO doit etre une chaine de au plus 19 caractere
                If ligne(i).Length > 19 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier ligne a une longueur invalide")
                Else
                    aLigne.LIGNE_NO = ligne(i)
                End If
                i = i + 1

                'le champs AR_REF doit etre une chaine de au plus 19 caractere
                If ligne(i).Length > 19 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier ligne a une longueur invalide")
                Else
                    aLigne.AR_REF = ligne(i)
                End If
                i = i + 1


                'le  champs DL_QTE doit etre un double
                If Not Double.TryParse(ligne(i).Replace(",", "."), style, culture, aLigne.DL_QTE) Then
                    Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un double")
                End If
                i = i + 1

                'le  champs DL_PUNET doit etre un double
                If Not Double.TryParse(ligne(i).Replace(",", "."), style, culture, aLigne.DL_PUNET) Then
                    Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un double")
                End If
                i = i + 1



                'le  champs DL_REMISE doit etre un double ou null
                If ligne(i) = "" Then
                    aLigne.DL_REMISE = 0
                Else
                    If Not Double.TryParse(ligne(i).Replace(",", "."), style, culture, aLigne.DL_REMISE) Then
                        Throw New ChampsInvalide("Le champs " & i & " du fichier n'est pas un double")
                    End If
                End If
                i = i + 1

                'le champs AR_DESIGN doit etre une chaine de au plus 69 caractere
                If ligne(i).Length > 512 Then
                    Throw New LongeurChampsInvalide("Le champs " & i & " du fichier ligne a une longueur invalide")
                Else
                    aLigne.AR_DESIGN = Left(ligne(i), 69).Replace("""", "")
                End If
                i = i + 1

                LIGNES.Add(aLigne)

            End While


        Catch ex As Exception
            csvLigne.close()
            csvLigne = Nothing
            Throw ex
        Finally
            csvLigne.close()
            csvLigne = Nothing
            FileLigne = File_Ligne
        End Try

    End Sub



    'function d'insert de la commande dans sage

    'Public Function InsertToSage(ByVal BD As String, ByVal SQLConnectionString As String, Optional ByRef SpecialREf As List(Of String) = Nothing, Optional ByRef RPS As List(Of String) = Nothing) As Boolean
    '    Dim bret As Boolean
    '    Dim bfind As Boolean
    '    bfind = False
    '    bret = True
    '    If LIGNES.Count <> 0 Then
    '        Dim baseCommerciale As BSCIALApplication3 = Nothing

    '        Try

    '            baseCommerciale = OuvreBaseCommerciale(BD)


    '            Dim documentGestionCo As IPMDocument
    '            documentGestionCo = Nothing
    '            Dim typeDoc As Objets100Lib.DocumentType
    '            Select Case DO_TYPEWEB
    '                Case 0
    '                    typeDoc = DocumentType.DocumentTypeVenteDevis
    '                Case 1
    '                    typeDoc = DocumentType.DocumentTypeVenteCommande
    '                Case 2
    '                    typeDoc = DocumentType.DocumentTypeVenteLivraison
    '                Case 4
    '                    typeDoc = DocumentType.DocumentTypeVenteAvoir

    '                Case 10
    '                    typeDoc = DocumentType.DocumentTypeAchatCommande

    '                Case Else
    '                    Throw New TypeDeDocumentGerer("Le DO_TYPE = " & DO_TYPEWEB & " n'est pas geré")
    '                    bret = False

    '            End Select

    '            Select Case DO_TYPEWEB
    '                Case 0, 1, 2, 4
    '                    Dim LI_Intitule As String
    '                    LI_Intitule = ""
    '                    Try
    '                        Dim sqldb As New SQLAccesBases(SQLConnectionString)
    '                        sqldb.open()
    '                        LI_Intitule = sqldb.GetOneResultValue("Select LI_Intitule From F_LIVRAISON where CT_NUM='" & DO_TIERS & "' and LI_no=" & Li_NO)
    '                        sqldb.close()
    '                    Catch
    '                        Try
    '                            Dim sqldb As New SQLAccesBases(SQLConnectionString)
    '                            Dim nbadresse As Integer
    '                            sqldb.open()
    '                            nbadresse = sqldb.GetOneResultValue("Select count(LI_Intitule) From F_LIVRAISON where CT_NUM='" & DO_TIERS & "'")
    '                            If nbadresse = 1 Then
    '                                LI_Intitule = sqldb.GetOneResultValue("Select LI_Intitule From F_LIVRAISON where CT_NUM='" & DO_TIERS & "'")
    '                            Else
    '                                Throw New Li_no_Non_Trouver("Le li_no est introuvable dans la table")
    '                            End If
    '                            sqldb.close()
    '                        Catch

    '                            Throw New Li_no_Non_Trouver("Le li_no est introuvable dans la table")
    '                        End Try


    '                    End Try

    '                    If ENT_COM Like "PICTHEMA*" Then
    '                        Dim docsearch As IBODocumentVente3
    '                        Dim client As IBOClient3
    '                        Dim baseComptable As BSCPTAApplication3


    '                        docsearch = Nothing
    '                        baseComptable = baseCommerciale.CptaApplication
    '                        client = baseComptable.FactoryClient.ReadNumero(DO_TIERS)

    '                        For Each docsearch In baseCommerciale.FactoryDocumentVente.QueryTiersTypeOrderDate(client, DocumentType.DocumentTypeVenteCommande)
    '                            If docsearch.DO_Ref = ENT_COM Then bfind = True
    '                        Next
    '                        If bfind Then
    '                            'le document existe deja , on ne fait qu'ajouter les lignes
    '                            For Each l As Ligne In LIGNES
    '                                AjouteLigneSortieArticleLotSerie(docsearch, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, SpecialREf)
    '                            Next
    '                            strDO_PIECE = docsearch.DO_Piece
    '                            'on ne fait pas la validation du doc  (inutile le document est deja créé)
    '                            Console.WriteLine("Lignes ajoutés  avec succès ! " & docsearch.DO_Piece)

    '                        Else
    '                            documentGestionCo = CreeDocumentVentePicthema(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, ENT_COM, ENT_ESCPTE, LI_Intitule)
    '                            For Each l As Ligne In LIGNES
    '                                AjouteLigneSortieArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, SpecialREf)
    '                            Next
    '                            strDO_PIECE = documentGestionCo.Document.DO_Piece
    '                        End If
    '                        client = Nothing
    '                        baseComptable = Nothing
    '                        docsearch = Nothing


    '                    Else
    '                        documentGestionCo = CreeDocumentVente(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, ENTETE_NO, ENT_ESCPTE, LI_Intitule)
    '                        For Each l As Ligne In LIGNES
    '                            AjouteLigneSortieArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, SpecialREf)
    '                        Next
    '                        strDO_PIECE = documentGestionCo.Document.DO_Piece
    '                    End If


    '                Case 10
    '                    documentGestionCo = CreeDocumentAchat(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, "REAPRO " & DO_TIERS)
    '                    For Each l As Ligne In LIGNES
    '                        AjouteLigneEntreeArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE)
    '                    Next
    '                    strDO_PIECE = documentGestionCo.Document.DO_Piece


    '                Case Else
    '                    Throw New TypeDeDocumentGerer("Le DO_TYPE = " & DO_TYPEWEB & " n'est pas geré")
    '                    bret = False

    '            End Select
    '            If Not bfind Then
    '                ValideDocument(documentGestionCo)
    '                'Attention on modifie les infolibre uniquement sur des document valide
    '                ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "Divers4", Left(ENT_COM, 39))
    '                ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "Divers1", "")
    '                'Application des modeles d'enregitrement.

    '                Select Case DO_TYPEWEB
    '                    Case 0, 1, 2, 4
    '                        If Not AppliqueModeleEnregistrementVente(baseCommerciale, documentGestionCo.DocumentResult) Then
    '                            'TODO efface la piece
    '                        End If
    '                End Select
    '                Console.WriteLine("Document de vente créé avec succès ! " & documentGestionCo.DocumentResult.DO_Piece & " " & ENT_ESCPTE)
    '            End If


    '        Catch ex As Exception

    '            Console.WriteLine(ex.Message)
    '            bret = False
    '        Finally

    '            FermeBaseCommerciale(baseCommerciale)

    '        End Try

    '        ' Console.Read()



    '        Return bret
    '    Else
    '        Return False
    '    End If

    'End Function
    Public Function InsertToSage(ByRef BD As BaseCommercial, ByRef conf As Configuration) As Boolean
        Dim bret As Boolean
        Dim bfind As Boolean
        Dim email As String

        bfind = False
        bret = True
        If LIGNES.Count <> 0 Then
            Dim baseCommerciale As BSCIALApplication3 = Nothing

            Try

                baseCommerciale = Magestion.Ouvre


                Dim documentGestionCo As IPMDocument
                documentGestionCo = Nothing
                Dim typeDoc As Objets100Lib.DocumentType
                Select Case DO_TYPEWEB
                    Case 0
                        typeDoc = DocumentType.DocumentTypeVenteDevis
                    Case 1
                        typeDoc = DocumentType.DocumentTypeVenteCommande
                    Case 2
                        typeDoc = DocumentType.DocumentTypeVenteLivraison
                    Case 4
                        typeDoc = DocumentType.DocumentTypeVenteAvoir

                    Case 10
                        typeDoc = DocumentType.DocumentTypeAchatCommande

                    Case Else
                        Throw New TypeDeDocumentGerer("Le DO_TYPE = " & DO_TYPEWEB & " n'est pas geré")
                        bret = False

                End Select

                Select Case DO_TYPEWEB
                    Case 0, 1, 2, 4
                        Dim LI_Intitule As String
                        LI_Intitule = ""
                        Try
                            Dim sqldb As New SQLAccesBases(conf.SQLConnectionString)
                            sqldb.open()
                            LI_Intitule = sqldb.GetOneResultValue("Select LI_Intitule From F_LIVRAISON where CT_NUM='" & DO_TIERS & "' and LI_no=" & Li_NO)
                            sqldb.close()
                        Catch
                            Try
                                Dim sqldb As New SQLAccesBases(conf.SQLConnectionString)
                                Dim nbadresse As Integer
                                sqldb.open()
                                nbadresse = sqldb.GetOneResultValue("Select count(LI_Intitule) From F_LIVRAISON where CT_NUM='" & DO_TIERS & "'")
                                If nbadresse = 1 Then
                                    LI_Intitule = sqldb.GetOneResultValue("Select LI_Intitule From F_LIVRAISON where CT_NUM='" & DO_TIERS & "'")
                                Else
                                    Throw New Li_no_Non_Trouver("Le li_no est introuvable dans la table")
                                End If
                                sqldb.close()
                            Catch

                                Throw New Li_no_Non_Trouver("Le li_no est introuvable dans la table")
                            End Try


                        End Try

                        If ENT_COM Like "PICTHEMA*" Then
                            Dim docsearch As IBODocumentVente3
                            Dim client As IBOClient3
                            Dim baseComptable As BSCPTAApplication3


                            docsearch = Nothing
                            baseComptable = baseCommerciale.CptaApplication
                            client = baseComptable.FactoryClient.ReadNumero(DO_TIERS)

                            For Each docsearch In baseCommerciale.FactoryDocumentVente.QueryTiersTypeOrderDate(client, DocumentType.DocumentTypeVenteCommande)
                                If docsearch.DO_Ref = ENT_COM Then
                                    bfind = True
                                    Exit For
                                End If

                            Next
                            If bfind Then
                                'le document existe deja , on ne fait qu'ajouter les lignes
                                For Each l As Ligne In LIGNES
                                    AjouteLigneSortieArticleLotSerie(docsearch, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, conf.SpecialRef)
                                Next
                                'on ne fait pas la validation du doc  (inutile le document est deja créé)
                                Console.WriteLine("Lignes ajoutés  avec succès ! " & docsearch.DO_Piece)

                            Else
                                documentGestionCo = CreeDocumentVentePicthema(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, ENT_COM, ENT_ESCPTE, LI_Intitule)
                                For Each l As Ligne In LIGNES
                                    AjouteLigneSortieArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, conf.SpecialRef)
                                Next
                            End If
                            client = Nothing
                            baseComptable = Nothing
                            docsearch = Nothing


                        Else
                            documentGestionCo = CreeDocumentVente(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, ENTETE_NO, ENT_ESCPTE, LI_Intitule, ENT_LIVRAISON, ENT_ENC_MODE, DO_COORD01, DO_COORD02, DO_COORD03, DO_COORD04)
                            For Each l As Ligne In LIGNES
                                If l.AR_REF Like "UNAL*" Or _
                                    l.AR_REF = "CADECOMIX" Or _
                                    l.AR_REF = "PSCG" Or _
                                    l.AR_REF = "PW" Or _
                                    l.AR_REF Like "PW#" Or _
                                    l.AR_REF = "RFA_2T00" Or _
                                    l.AR_REF Like "RPP*" Or _
                                    l.AR_REF Like "ZPICTHEMA" Then
                                    'on met la designation exporté par le MBE  il contient la couleur le nombre de page ....
                                    AjouteLigneSortieArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE, , l.AR_DESIGN, l.DL_PUNET, l.DL_REMISE, conf.SpecialRef)
                                Else
                                    'on prend la designation de sage
                                    AjouteLigneSortieArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE, , "", l.DL_PUNET, l.DL_REMISE, conf.SpecialRef)
                                End If
                            Next
                        End If


                    Case 10
                        documentGestionCo = CreeDocumentAchat(baseCommerciale, typeDoc, DO_TIERS, DO_DATE, "REAPRO " & DO_TIERS)
                        For Each l As Ligne In LIGNES
                            AjouteLigneEntreeArticleLotSerie(documentGestionCo, l.AR_REF, l.DL_QTE)
                        Next

                    Case Else
                        Throw New TypeDeDocumentGerer("Le DO_TYPE = " & DO_TYPEWEB & " n'est pas geré")
                        bret = False

                End Select
                If Not bfind Then
                    ValideDocument(documentGestionCo)
                    'Attention on modifie les infolibre uniquement sur des document valide
                    ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "Divers4", Left(ENT_COM, 39))
                    ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "Divers1", "")
                    ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "ReferencePaiementEnLigne", Left(ReferencePaiementEnLigne, 20))
                    If Libre3 <> "" Then
                        ModifieInfoLibreEntete(documentGestionCo.DocumentResult, "libre3", Libre3)
                    End If


                    'Application des modeles d'enregitrement.

                    Select Case DO_TYPEWEB
                        Case 1 'bon de commande
                            AppliqueModeleEnregistrementVente(baseCommerciale, documentGestionCo.DocumentResult)
                            bConfirme = ConfirmeCommande(baseCommerciale, documentGestionCo.DocumentResult, DO_TIERS, ENT_COM, conf)
                        Case 0, 2, 4
                            If Not AppliqueModeleEnregistrementVente(baseCommerciale, documentGestionCo.DocumentResult) Then
                                'If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {"n.molders@mbeditions.fr"}, "Erreur dans une piece pour le " & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece, "Impossible d'execuetre les modeles d'enregistrement", {"mail.automatique@mbtech.fr"}) Then
                                If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {"n.molders@mbeditions.fr"}, "Erreur dans une piece pour le " & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece, "Impossible d'execuetre les modeles d'enregistrement") Then
                                    log.AddLog("Message non envoyé : n.molders@mbeditions.fr-> Erreur dans une piece pour le " & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece & ">>>" & "Impossible d'execuetre les modeles d'enregistrement")

                                End If

                            End If
                            If IsNothing(documentGestionCo.DocumentResult.Collaborateur) Then
                                email = conf.DefaultMail
                            Else
                                email = documentGestionCo.DocumentResult.Collaborateur.Service
                            End If


                            If Not Mail.IsCorrectEmail(email) Then email = conf.DefaultMail
                            'If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {email}, "Integration pour le" & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece, ENT_COM, {"mail.automatique@mbtech.fr"}) Then
                            If Not globalmail.SendMailWithRetry("mail.automatique@mbtech.fr", {email}, "Integration pour le" & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece, ENT_COM) Then
                                log.AddLog("Message non envoyé : " & email & "-> " & "Integration pour le" & DO_TIERS & " " & documentGestionCo.DocumentResult.DO_Piece & ">>>" & ENT_COM)


                            End If
                    End Select





                    Console.WriteLine("Document de vente créé avec succès ! " & documentGestionCo.DocumentResult.DO_Piece & " " & ENT_ESCPTE.ToString)
                End If



            Catch ex As Exception

                Console.WriteLine(ex.Message)
                Throw ex
                bret = False
            Finally
                BD.Ferme()


            End Try

            ' Console.Read()



            Return bret
        Else
            Throw New LigneIntrouvable("Aucune Lignes !!!")
            Return False
        End If

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class


Public Class Ligne
    Property LIGNE_NO As String
    Property AR_REF As String
    Property DL_PUNET As Double
    Property DL_QTE As Double
    Property DL_REMISE As Double
    Property AR_DESIGN As String
End Class