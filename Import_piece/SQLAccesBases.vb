﻿Imports System.Data.SqlClient
Public Class SQLAccesBases
    Public Shared Sub Main()

        Try
            'Chaîne de connexion
            Dim connectString As String = "database=L100_MBTECH;server=SRVSAGE"
            'Objet connection
            Dim connection As SqlConnection = New SqlConnection(connectString)
            'Ouverture
            connection.Open()
            'Fermeture
            connection.Close()
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try

    End Sub

    Property connection As SqlConnection

    'constructeur

    'constructeur  par defaut

    Public Sub New(ByVal connectionString As String)
        connection = New SqlConnection(connectionString)
    End Sub

    Public Sub open()
        connection.Open()
    End Sub

    Public Sub close()
        connection.Close()
    End Sub

    Public Function GetOneResultValue(ByVal req As String)


        Dim command As SqlCommand = New SqlCommand(req, connection)
        Dim reader As SqlDataReader = command.ExecuteReader
        Dim row As Object() = Nothing

        If (reader.Read) Then
            If row Is Nothing Then
                row = New Object(reader.FieldCount) {}
            End If
            reader.GetValues(row)
            If reader.FieldCount <> 1 Then
                reader.Close()
                GetOneResultValue = ""
                Throw New Exception("1 seul champs dois etre retourné")
            Else
                If Not row(0) Is Nothing AndAlso Not (row(0) Is DBNull.Value) Then
                    GetOneResultValue = row(0)
                Else
                    GetOneResultValue = DBNull.Value
                End If

            End If
        Else
            reader.Close()
            Throw New Exception("la requette dois renvoyer au moins une ligne")
        End If
        reader.Close()



    End Function




    Protected Overrides Sub Finalize()
        Try
            close()
        Catch ex As Exception

        End Try

        MyBase.Finalize()
    End Sub

End Class
