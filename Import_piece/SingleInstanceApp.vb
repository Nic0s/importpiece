﻿
Imports System.Threading

Class SingleInstanceApp
    Implements IDisposable

    Private _siMutex As Mutex
    Private _siMutexOwned As Boolean

    Public Sub New(ByVal name As String)
        _siMutex = New Mutex(False, name)
        _siMutexOwned = False
    End Sub

    Public Function IsRunning() As Boolean
        _siMutexOwned = _siMutex.WaitOne(0, True)
        Return Not (_siMutexOwned)
    End Function

    Public Sub Dispose() Implements System.IDisposable.Dispose
        If _siMutexOwned Then
            _siMutex.ReleaseMutex()
        End If
    End Sub

End Class

