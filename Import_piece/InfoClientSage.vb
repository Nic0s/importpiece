﻿Imports Objets100Lib

Module InfoClientSage
   
    Function IsClientExport(ByVal CT_NUM As String, ByRef baseCommerciale As BSCIALApplication3) As Boolean
        Dim client As IBOClient3

        Dim baseComptable As BSCPTAApplication3
        baseComptable = baseCommerciale.CptaApplication
        Dim rp As IBOCollaborateur
        rp = baseComptable.FactoryCollaborateur.ReadNomPrenom("EXPORT", "")
        client = baseComptable.FactoryClient.ReadNumero(CT_NUM.ToUpper)
        IsClientExport = (client.Representant Is rp)
        rp = Nothing
        client = Nothing


        baseComptable = Nothing
    End Function
    Function IsClientExport(ByVal CT_NUM As String, ByRef BD As BaseCommercial) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = BD.Ouvre
            IsClientExport = IsClientExport(CT_NUM, baseCommerciale)
            BD.Ferme()
        Catch e As Exception
            Return False
        End Try
    End Function
    Function IsClientExport(ByVal CT_NUM As String, ByRef BD As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = OuvreBaseCommerciale(BD)
            IsClientExport = IsClientExport(CT_NUM, baseCommerciale)
            FermeBaseCommerciale(baseCommerciale)
        Catch e As Exception
            Return False
        End Try
    End Function

    Function IsClientGrandCompte(ByVal CT_NUM As String, ByRef baseCommerciale As BSCIALApplication3) As Boolean
        Dim client As IBOClient3
        Dim baseComptable As BSCPTAApplication3
        baseComptable = baseCommerciale.CptaApplication
        Dim rp As IBOCollaborateur
        rp = baseComptable.FactoryCollaborateur.ReadNomPrenom("COMPTE CLE", "")
        client = baseComptable.FactoryClient.ReadNumero(CT_NUM.ToUpper)
        IsClientGrandCompte = (client.Representant Is rp)
        rp = Nothing
        client = Nothing


        baseComptable = Nothing


    End Function
    Function IsClientGrandCompte(ByVal CT_NUM As String, ByRef BD As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = OuvreBaseCommerciale(BD)
            IsClientGrandCompte = IsClientGrandCompte(CT_NUM, baseCommerciale)
            FermeBaseCommerciale(baseCommerciale)
        Catch e As Exception
            Return False
        End Try
    End Function
    Function IsClientGrandCompte(ByVal CT_NUM As String, ByRef BD As BaseCommercial) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = BD.Ouvre
            IsClientGrandCompte = IsClientGrandCompte(CT_NUM, baseCommerciale)
            BD.Ferme()
        Catch e As Exception
            Return False
        End Try
    End Function

    Function IsClientComptant(ByVal CT_NUM As String, ByRef baseCommerciale As BSCIALApplication3, ByRef strMessage As String) As Boolean
        Dim client As IBOClient3

        Dim baseComptable As BSCPTAApplication3
        IsClientComptant = False
        baseComptable = baseCommerciale.CptaApplication
        client = baseComptable.FactoryClient.ReadNumero(CT_NUM.ToUpper)
        Dim regl As IBIReglement
        regl = client.FactoryTiersReglement().List(1)
        'on regarde le premier enregistrement vu que les reglement sont trié en croissant
        If regl.NbJour <= 1 Then

            If regl.Reglement.R_Intitule <> "Prélvt (P)" And regl.Reglement.R_Intitule <> "En ligne" Then

                'pour que la commande parte alors que le reglmetn est à 0 ou 1 jour , seul le mode prelevement est autorisé
                IsClientComptant = True
                strMessage = "Le mode de reglement necessite un traitement manuel (intitulé réglement = {" & regl.Reglement.R_Intitule & "})"
            End If
        End If


        client = Nothing
        regl = Nothing


        baseComptable = Nothing


    End Function
    Function IsClientComptant(ByVal CT_NUM As String, ByRef BD As String, ByRef strMessage As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = OuvreBaseCommerciale(BD)
            IsClientComptant = IsClientComptant(CT_NUM, baseCommerciale, strMessage)
            FermeBaseCommerciale(baseCommerciale)
        Catch e As Exception
            Return False
        End Try
    End Function
    Function IsClientComptant(ByVal CT_NUM As String, ByRef BD As BaseCommercial, ByRef strMessage As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = BD.Ouvre
            IsClientComptant = IsClientComptant(CT_NUM, baseCommerciale, strMessage)
            BD.Ferme()
        Catch e As Exception
            Return False
        End Try
    End Function

    Function IsClientBlocked(ByVal CT_NUM As String, ByRef baseCommerciale As BSCIALApplication3)
        Dim client As IBOClient3

        Dim baseComptable As BSCPTAApplication3
        IsClientBlocked = False
        baseComptable = baseCommerciale.CptaApplication
        client = baseComptable.FactoryClient.ReadNumero(CT_NUM.ToUpper)
      
        Select Case client.CT_ControlEnc
            Case ClientEncoursCtrlType.ClientEncoursCtrlTypeBloque
                IsClientBlocked = True
            Case ClientEncoursCtrlType.ClientEncoursCtrlTypeCodeRisque
                If client.CodeRisque.R_Intitule = "A Bloquer" Then
                    IsClientBlocked = True
                End If

        End Select


        

        client = Nothing



        baseComptable = Nothing

    End Function
    Function IsClientBlocked(ByVal CT_NUM As String, ByRef BD As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = OuvreBaseCommerciale(BD)
            IsClientBlocked = IsClientBlocked(CT_NUM, baseCommerciale)
            FermeBaseCommerciale(baseCommerciale)
        Catch e As Exception
            Return False
        End Try
    End Function
    Function IsClientBlocked(ByVal CT_NUM As String, ByRef BD As BaseCommercial) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = BD.Ouvre
            IsClientBlocked = IsClientBlocked(CT_NUM, baseCommerciale)
            BD.Ferme()
        Catch e As Exception
            Return False
        End Try
    End Function


    Function IsClientGMS(ByVal CT_NUM As String, ByRef baseCommerciale As BSCIALApplication3)
        Dim client As IBOClient3

        Dim baseComptable As BSCPTAApplication3
        IsClientGMS = False
        baseComptable = baseCommerciale.CptaApplication
        client = baseComptable.FactoryClient.ReadNumero(CT_NUM.ToUpper)

  
        If client.InfoLibre.Item("ACTIVITE1") = "GS" Then
            IsClientGMS = True
        End If

        Select Case UCase(client.InfoLibre.Item("ENSEIGNE1"))
            Case "LE", "IM", "SU", "VI"
                IsClientGMS = True
        End Select


        client = Nothing



        baseComptable = Nothing

    End Function
    Function IsClientGMS(ByVal CT_NUM As String, ByRef BD As String) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = OuvreBaseCommerciale(BD)
            IsClientGMS = IsClientGMS(CT_NUM, baseCommerciale)
            FermeBaseCommerciale(baseCommerciale)
        Catch e As Exception
            Return False
        End Try
    End Function
    Function IsClientGMS(ByVal CT_NUM As String, ByRef BD As BaseCommercial) As Boolean

        Dim baseCommerciale As BSCIALApplication3 = Nothing


        Try
            baseCommerciale = BD.Ouvre
            IsClientGMS = IsClientGMS(CT_NUM, baseCommerciale)
            BD.Ferme()
        Catch e As Exception
            Return False
        End Try
    End Function
End Module
