﻿Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading

Public Class Mail
    Shared Sub sendmail(ByVal strfrom As String, _
                        ByVal strTo As String, _
                        ByVal strsujet As String, _
                        ByVal strbody As String, _
                        ByVal strSMTP As String, _
                        ByVal iport As Integer, _
                        ByVal bssl As Boolean, _
                        ByVal strLogin As String, _
                        ByVal strpass As String)
        Dim MyMailMessage As New MailMessage()
        Try
            MyMailMessage.From = New MailAddress(strfrom)
            MyMailMessage.To.Add(strTo)
            MyMailMessage.Subject = strsujet
            MyMailMessage.Body = strbody
            Dim SMTP As New SmtpClient(strSMTP)
            SMTP.Port = iport
            SMTP.EnableSsl = bssl
            If strlogin <> "" Then SMTP.Credentials = New System.Net.NetworkCredential(strLogin, strPass)
            '"From E-Mail Adress username and password"

            SMTP.Send(MyMailMessage)

            MsgBox("Message Envoyé!")
        Catch ex As Exception
             MsgBox("Message NON Envoyé!")
        End Try
    End Sub

    Private strSMTP As String
    Private iPort As Integer
    Private bSsl As Boolean
    Private strLogin As String
    Private strPass As String

    Sub New(ByVal strSMTP_ As String, _
                        ByVal iPort_ As Integer, _
                        ByVal bSsl_ As Boolean, _
                        ByVal strLogin_ As String, _
                        ByVal strPass_ As String)
        strSMTP = strSMTP_
        iPort = iPort_
        bSsl = bSsl_
        strLogin = strLogin_
        strPass = strPass_

    End Sub

    Public Function SendMailWithRetry(ByVal strFrom As String, _
                        ByVal strTo As String(), _
                        ByVal strsujet As String, _
                        ByVal strbody As String, _
                        Optional ByVal strBCC As String() = Nothing, _
                        Optional ByVal strPieceJointe As String() = Nothing, _
                        Optional ByVal Maxtry As Integer = 5) As Boolean

        Dim nbtry As Integer = 0
        Dim stopwatch As Stopwatch
        Dim bret As Boolean = False


        Do

            bret = sendmail(strFrom, strTo, strsujet, strbody, strBCC, strPieceJointe)
            If bret Then
                Exit Do
            Else
                stopwatch = stopwatch.StartNew
                Thread.Sleep(5000)
                stopwatch.Stop()
                nbtry = nbtry + 1
            End If
        Loop Until nbtry > Maxtry
        Return bret

    End Function


    Public Function SendMail(ByVal strFrom As String, _
                        ByVal strTo As String(), _
                        ByVal strsujet As String, _
                        ByVal strbody As String, _
                        Optional ByVal strBCC As String() = Nothing, _
                        Optional ByVal strPieceJointe As String() = Nothing) As Boolean







        Dim MyMailMessage As New MailMessage()
        Try
            MyMailMessage.From = New MailAddress(strFrom)

            For Each adr As String In strTo
                MyMailMessage.To.Add(adr)
            Next

            If Not IsNothing(strBCC) Then
                For Each adr As String In strBCC
                    MyMailMessage.Bcc.Add(adr)
                Next
            End If
            'MyMailMessage.Bcc.Add("n.molders@mbeditions.fr, f.de-foy@mbtech.fr")


            If Not IsNothing(strPieceJointe) Then
                Dim pieceJointe As Attachment
                Dim PieceJointeInfo As ContentDisposition

                For Each strFile As String In strPieceJointe
                    Try
                        'pieceJointe = New Attachment(strFile, MediaTypeNames.Application.Octet)
                        pieceJointe = New Attachment(strFile)
                        pieceJointe.Name = strFile
                        pieceJointe.NameEncoding = Encoding.UTF8
                        PieceJointeInfo = pieceJointe.ContentDisposition
                        PieceJointeInfo.CreationDate = System.IO.File.GetCreationTime(strFile)

                        PieceJointeInfo.ModificationDate = System.IO.File.GetLastWriteTime(strFile)
                        PieceJointeInfo.ReadDate = System.IO.File.GetLastAccessTime(strFile)
                        MyMailMessage.Attachments.Add(pieceJointe)
                    Catch ex As Exception
                        SendMail = False
                        Exit Function
                    End Try


                Next
            End If


            MyMailMessage.Subject = strsujet
            MyMailMessage.Body = strbody


            Dim SMTP As New SmtpClient(strSMTP)
            SMTP.Port = iPort
            SMTP.EnableSsl = bSsl
            If strLogin <> "" Then SMTP.Credentials = New System.Net.NetworkCredential(strLogin, strPass)


            SMTP.Send(MyMailMessage)

            SendMail = True
        Catch ex As Exception

            SendMail = False
        End Try




    End Function



    Public Shared Function IsCorrectEmail(ByVal emailAddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            IsCorrectEmail = True
        Else
            IsCorrectEmail = False
        End If
    End Function





End Class
