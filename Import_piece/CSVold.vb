﻿
Imports System.Text.RegularExpressions
Imports System.IO

Public Class CSV
    Public Shared Function DecodeCSV(ByVal strLine As String) As String()

        Dim strPattern As String
        Dim objMatch As Match

        ' build a pattern
        strPattern = "^" ' anchor to start of the string
        strPattern += "(?:""(?<value>(?:""""|[^""\f\r])*)""|(?<value>[^,\f\r""]*))"
        strPattern += "(?:,(?:[ \t]*""(?<value>(?:""""|[^""\f\r])*)""|(?<value>[^,\f\r""]*)))*"
        strPattern += "$" ' anchor to the end of the string

        ' get the match
        objMatch = Regex.Match(strLine, strPattern)

        ' if RegEx match was ok
        If objMatch.Success Then
            Dim objGroup As Group = objMatch.Groups("value")
            Dim intCount As Integer = objGroup.Captures.Count
            Dim arrOutput(intCount - 1) As String

            ' transfer data to array
            For i As Integer = 0 To intCount - 1
                Dim objCapture As Capture = objGroup.Captures.Item(i)
                arrOutput(i) = objCapture.Value

                ' replace double-escaped quotes
                arrOutput(i) = arrOutput(i).Replace("""""", """")
            Next

            ' return the array
            Return arrOutput
        Else
            Throw New ApplicationException("Bad CSV line: " & strLine)
        End If

    End Function

    Private _StrFile As String
    Private _lecteur As StreamReader



    'Coinstructeur par defaut
    Public Sub New()
        _lecteur = Nothing
    End Sub

    'constructeur avec un fichier à ouvrir en parametre
    Public Sub New(ByVal Strfile As String)
        MyClass.New()
        _StrFile = Strfile
        Try
            'path est le chemin complet du fichier à lire 
            _lecteur = New StreamReader(_StrFile, System.Text.Encoding.ASCII)
            _lecteur.BaseStream.Seek(0, SeekOrigin.Begin)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Throw ex
        End Try
    End Sub

    Public Function ReadNextLine() As String()
        Dim s As String() = Nothing
        Dim ligne As String
        Try
            ligne = _lecteur.ReadLine
            s = DecodeCSV(ligne)

        Catch ex As Exception
            Console.WriteLine(ex.Message)

        Finally
        End Try
        Return s

    End Function

    Public Function EOF() As Boolean
        Dim ret As Boolean
        Try
            ret = (_lecteur.Peek <= 0)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

        End Try
        Return ret
    End Function

    Public Function ReadAllLine() As ArrayList
        'DECLARER un lecteur de fichier 
        Dim monlecteur As StreamReader = Nothing
        'DECLARER une collection pour les lignes du fichier 
        Dim meslignes As New ArrayList
        Try
            'path est le chemin complet du fichier à lire 
            monlecteur = New StreamReader(_StrFile)
            With monlecteur
                'POSITIONNER le flux au début du fichier 
                .BaseStream.Seek(0, SeekOrigin.Begin)
                'TANT QUE le lecteur a quelquechose à lire 
                While (.Peek > 0)
                    'LIRE une ligne du fichier et l'ajouter à meslignes 
                    meslignes.Add(.ReadLine)
                End While
            End With
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            'FERMER le lecteur en tout etat de cause 
            If Not IsNothing(monlecteur) Then
                monlecteur.Close()
            End If
        End Try
        Return meslignes
    End Function

    Public Shared Function ReadAllLine(ByVal path As String) As ArrayList
        'DECLARER un lecteur de fichier 
        Dim monlecteur As StreamReader = Nothing
        'DECLARER une collection pour les lignes du fichier 
        Dim meslignes As New ArrayList
        Try
            'path est le chemin complet du fichier à lire 
            monlecteur = New StreamReader(path)
            With monlecteur
                'POSITIONNER le flux au début du fichier 
                .BaseStream.Seek(0, SeekOrigin.Begin)
                'TANT QUE le lecteur a quelquechose à lire 
                While (.Peek > 0)
                    'LIRE une ligne du fichier et l'ajouter à meslignes 
                    meslignes.Add(.ReadLine)
                End While
            End With
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            'FERMER le lecteur en tout etat de cause 
            If Not IsNothing(monlecteur) Then
                monlecteur.Close()
            End If
        End Try
        Return meslignes
    End Function

    Public Sub close()
        If Not IsNothing(_lecteur) Then
            _lecteur.Close()
        End If
        _lecteur = Nothing
    End Sub

    'Destructeur
    Protected Overrides Sub Finalize()
        'FERMER le lecteur en tout etat de cause 
        MyClass.close()
    End Sub

    Sub test()
        Using MyReader As New Microsoft.VisualBasic.FileIO.
    TextFieldParser("c:\logs\bigfile")

            MyReader.TextFieldType =
                Microsoft.VisualBasic.FileIO.FieldType.Delimited


            MyReader.Delimiters = New String() {","}
            '  MyReader.CommentTokens = New String() {""}
            MyReader.HasFieldsEnclosedInQuotes = True


            Dim currentRow As String()
            'Loop through all of the fields in the file. 
            'If any lines are corrupt, report an error and continue parsing. 
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    ' Include code here to handle the row.
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message &
                    " is invalid.  Skipping")
                End Try
            End While
        End Using
    End Sub

End Class

