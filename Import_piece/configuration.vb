﻿Imports System.Xml
Public Class Rp
    Public nom As String
    Public numero As Integer
    Public bal_assistante As String
End Class
Public Class RepScan
    Public nom As String
    Public chemin As String
End Class

Public Class Configuration


    'utilisation d'une arrayliste pour memoriser tous les RP
    Property RPs As List(Of Rp)
    Property RepToScan As List(Of RepScan)
    Property Base As String
    Property SavDir As String
    Property SQLConnectionString As String
    Property LogDir As String
    Property LogPrefix As String
    Property SpecialRef As List(Of String)
    Property ArticleAConfimer As List(Of String)
    Property DefaultMail As String
    Property MailSMTP As String
    Property MailLogin As String
    Property MailPass As String
    Property MailSsl As Boolean
    Property Mailport As Integer


    'constructeur  par defaut
    Public Sub New()
        RPs = New List(Of Rp)
        RepToScan = New List(Of RepScan)
        SpecialRef = New List(Of String)
        ArticleAConfimer = New List(Of String)
        Base = ""
        SavDir = ""
        MailSMTP = ""
        MailLogin = ""
        MailPass = ""
        DefaultMail = "n.molders@mbeditions.fr"
        MailSsl = False
        Mailport = 25

    End Sub
    'constructeur 
    Public Sub New(ByVal XMLFile As String)
        'on initilise les valeurs
        MyClass.New()
        ' on lit le xml

        'creation d'une nouvelle instance du membre xmldocument
        Dim XmlDoc As XmlDocument = New XmlDocument()

        XmlDoc.Load(My.Application.Info.DirectoryPath & "\" & XMLFile)
        Dim element As XmlNodeList
        Dim noeud, noeudEnf As XmlNode
        Dim newnom As RepScan
        Dim InfoConnectMAil As XmlElement

        If XmlDoc.DocumentElement.GetElementsByTagName("BASE").Count = 1 Then
            Base = XmlDoc.DocumentElement.GetElementsByTagName("BASE").Item(0).FirstChild.Value
            Base = Base.Trim()
        Else
            Base = ""
        End If
        Dim tmp As String
        If XmlDoc.DocumentElement.GetElementsByTagName("InfoConnectMAil").Count = 1 Then
            InfoConnectMAil = XmlDoc.DocumentElement.GetElementsByTagName("InfoConnectMAil").Item(0)
            If InfoConnectMAil.GetElementsByTagName("SMTP").Count = 1 Then
                MailSMTP = InfoConnectMAil.GetElementsByTagName("SMTP").Item(0).FirstChild.Value
                MailSMTP = MailSMTP.Trim()
            End If
            If InfoConnectMAil.GetElementsByTagName("port").Count = 1 Then
                tmp = InfoConnectMAil.GetElementsByTagName("port").Item(0).FirstChild.Value
                If IsNumeric(tmp) Then Mailport = Integer.Parse(tmp.Trim())
            End If
            If InfoConnectMAil.GetElementsByTagName("SSL").Count = 1 Then
                tmp = InfoConnectMAil.GetElementsByTagName("SSL").Item(0).FirstChild.Value

                Boolean.TryParse(tmp.Trim(), MailSsl)
            End If
            If InfoConnectMAil.GetElementsByTagName("Login").Count = 1 Then
                MailLogin = InfoConnectMAil.GetElementsByTagName("Login").Item(0).FirstChild.Value
                MailLogin = MailLogin.Trim()
            End If
            If InfoConnectMAil.GetElementsByTagName("PassWord").Count = 1 Then
                MailPass = InfoConnectMAil.GetElementsByTagName("PassWord").Item(0).FirstChild.Value
                MailPass = MailPass.Trim()
            End If

        End If

        If XmlDoc.DocumentElement.GetElementsByTagName("SQLConnectionString").Count = 1 Then
            SQLConnectionString = XmlDoc.DocumentElement.GetElementsByTagName("SQLConnectionString").Item(0).FirstChild.Value
            SQLConnectionString = SQLConnectionString.Trim()
        Else
            SQLConnectionString = ""
        End If

        If XmlDoc.DocumentElement.GetElementsByTagName("SAVDIRECTORY").Count = 1 Then
            SavDir = XmlDoc.DocumentElement.GetElementsByTagName("SAVDIRECTORY").Item(0).FirstChild.Value
            SavDir = SavDir.Trim()
            If Right(SavDir, 1) <> "\" Then SavDir = SavDir & "\"

        Else
            SavDir = ""
        End If

        If XmlDoc.DocumentElement.GetElementsByTagName("LogDirectory").Count = 1 Then
            LogDir = XmlDoc.DocumentElement.GetElementsByTagName("LogDirectory").Item(0).FirstChild.Value
            LogDir = LogDir.Trim()
            If Right(LogDir, 1) <> "\" Then LogDir = LogDir & "\"
        Else
            LogDir = ""
        End If

        If XmlDoc.DocumentElement.GetElementsByTagName("LogPrefix").Count = 1 Then
            LogPrefix = XmlDoc.DocumentElement.GetElementsByTagName("LogPrefix").Item(0).FirstChild.Value
            LogPrefix = LogPrefix.Trim()
        Else
            LogPrefix = ""
        End If


        element = XmlDoc.DocumentElement.GetElementsByTagName("REP_SCAN")
        For Each noeud In element
            newnom = New RepScan
            For Each noeudEnf In noeud.ChildNodes

                Select Case noeudEnf.LocalName
                    Case "REP"
                        newnom.chemin = noeudEnf.InnerText
                        newnom.chemin = newnom.chemin.Trim()
                    Case "NOM"
                        newnom.nom = noeudEnf.InnerText
                        newnom.nom = newnom.nom.Trim()

                End Select

            Next
            RepToScan.Add(newnom)
            newnom = Nothing
        Next

        Dim newRP As Rp
        element = XmlDoc.DocumentElement.GetElementsByTagName("REPRESENTANT")
        For Each noeud In element
            newRP = New Rp
            For Each noeudEnf In noeud.ChildNodes

                Select Case noeudEnf.LocalName
                    Case "NUMERO"
                        newRP.numero = noeudEnf.InnerText
                    Case "NOM"
                        newRP.nom = noeudEnf.InnerText
                        newRP.nom = newRP.nom.Trim()

                    Case "BAL_ASSISTANTE"
                        newRP.bal_assistante = noeudEnf.InnerText
                        newRP.bal_assistante = newRP.bal_assistante.Trim()

                End Select

            Next
            RPs.Add(newRP)
            newRP = Nothing
        Next

        element = XmlDoc.DocumentElement.GetElementsByTagName("SPECIAL_PRODUCT")
        Dim ref As String
        For Each noeud In element
            ref = noeud.InnerText
            ref = ref.Trim
            SpecialRef.Add(ref)
        Next


        element = XmlDoc.DocumentElement.GetElementsByTagName("ARTICLE_A_CONFIRMER")
        For Each noeud In element
            ref = noeud.InnerText
            ref = ref.Trim
            ArticleAConfimer.Add(ref)
        Next


        If XmlDoc.DocumentElement.GetElementsByTagName("DEFAULT_EMAIL").Count = 1 Then
            DefaultMail = XmlDoc.DocumentElement.GetElementsByTagName("DEFAULT_EMAIL").Item(0).FirstChild.Value
            DefaultMail = DefaultMail.Trim()
        End If

    End Sub

    Public Function findEmailfromRP(ByVal nom As String)
        Dim monrp As Rp

        For Each monrp In RPs
            If monrp.nom = nom Then Return monrp.bal_assistante
        Next
        findEmailfromRP = ""
    End Function

    'Destructeur
    Protected Overrides Sub Finalize()
        RPs = Nothing
        RepToScan = Nothing

    End Sub

End Class
