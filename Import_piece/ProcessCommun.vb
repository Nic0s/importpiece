﻿Imports Objets100Lib
Imports System.Text

Module ProcessCommun

    Function getErreursProcess(ByRef process As IPMProcess) As String

        Dim erreurs As New StringBuilder

        If process IsNot Nothing Then

            For Each erreur As IFailInfo In process.Errors
                erreurs.AppendLine(erreur.Text)
            Next

        End If

        Return erreurs.ToString()

    End Function

End Module
